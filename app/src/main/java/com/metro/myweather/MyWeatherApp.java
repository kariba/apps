package com.metro.myweather;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by ekkamau on 11/6/17.
 */

public class MyWeatherApp extends Application {

    MyWeatherComponent myWeatherComponent;

    public static MyWeatherApp get(Context context) {
        return (MyWeatherApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        initAppComponents();
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public MyWeatherComponent getAppComponent() {
        return myWeatherComponent;
    }

    public void initAppComponents() {

        myWeatherComponent = ComponentFactory.create();

    }
}
