package com.metro.myweather;

import com.metro.myweather.components.CurrentWeatherComponent;
import com.metro.myweather.modules.CurrentWeatherServiceModule;

/**
 * Created by ekkamau on 11/6/17.
 */

public interface MyWeatherComponent {

    CurrentWeatherComponent currentWeatherComponent(CurrentWeatherServiceModule currentWeatherModule);
}
