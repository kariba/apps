package com.metro.myweather;

import com.metro.myweather.modules.AppModule;
import com.metro.myweather.networking.ApiServicesModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ekkamau on 11/6/17.
 */

@Singleton
@Component(modules = {AppModule.class, ApiServicesModule.class})
public interface MainAppComponent extends MyWeatherComponent {

}
