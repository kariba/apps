package com.metro.myweather.networking;

/**
 * Created by ekkamau on 11/5/17.
 */

public class BaseResponse {
    long requestId;

    public BaseResponse(long requestId) {
        this.requestId = requestId;
    }

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }
}
