package com.metro.myweather.networking;


public class RetrofitErrorEvent {

    private long requestId;
    private String errorMessage;

    public RetrofitErrorEvent(long requestId, String errorMessage) {
        this.requestId = requestId;
        this.errorMessage = errorMessage;
    }

    public long getRid() {
        return requestId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
