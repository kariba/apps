package com.metro.myweather.networking;

import com.metro.myweather.currentweather.CurrentWeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ekkamau on 11/5/17.
 */

public interface IApiService {

    @GET("weather")
    Call<CurrentWeatherResponse> getCurrentWeatherByLatLong(@Query("lat") double latitude, @Query("lon") double longitude);
}
