package com.metro.myweather.networking;

/**
 * Created by ekkamau on 11/5/17.
 */

import android.util.Log;

import com.metro.myweather.events.GenericErrorEvent;
import com.squareup.otto.Bus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public abstract class RestCallback<T extends BaseResponse> implements Callback<T> {
    private static final int SESSION_ERROR = 405;
    private static final int INVLAID_IMSI = 409;
    private static final int CHANGE_PIN = 470;
    private String tag = this.getClass().getCanonicalName();
    private Bus bus;
    private long requestId;

    public RestCallback(T t, Bus bus, long requestId) {
        this.bus = bus;
        this.requestId = requestId;
        tag = t.getClass().getCanonicalName();
    }

    public RestCallback(Bus bus, long requestId) {
        this.bus = bus;
        this.requestId = requestId;
    }

    @Override
    public void onResponse(Call<T> t, Response<T> response) {
        Log.d(tag, "success");
        try {
            if (t != null) {

            }

            bus.post(t);
            Log.d(tag, response.body().toString());

        } catch (Exception e) {
            e.printStackTrace();
            bus.post(new RetrofitErrorEvent(requestId, "No response"));
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        bus.post(new GenericErrorEvent(t.getMessage()));
    }
}