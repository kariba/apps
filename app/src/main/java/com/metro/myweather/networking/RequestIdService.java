package com.metro.myweather.networking;

/**
 * Created by ekkamau on 11/6/17.
 */

public class RequestIdService {
    private static long mId = 0;

    public synchronized static long getNewRequestId() {
        long id = mId;
        mId += 1;
        return id;
    }

    public synchronized static long getRequestId() {
        return mId;
    }

    public synchronized static void resetCounter() {
        mId = 0;
    }
}
