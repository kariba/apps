package com.metro.myweather.networking;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.metro.myweather.BuildConfig;

import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiServicesModule {

    public static final String TAG = ApiServicesModule.class.getSimpleName();
    private static final String API_KEY = "appid";

    public ApiServicesModule() {

    }

    @Provides
    @Singleton
    public IApiService provideApiService(@Named("auth") Retrofit retrofit) {
        return retrofit.create(IApiService.class);
    }


    @Provides
    @Singleton
    @Named("auth")
    public Retrofit provideAuthAdapter(@Named("keyed") OkHttpClient client) {
        System.out.println(BuildConfig.DEFAULT_BASE_URL + " is debug: " + BuildConfig.DEBUG);
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .serializeNulls()
                .create();
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.DEFAULT_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Named("keyed")
    public OkHttpClient provideHttpClient() {
        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter(API_KEY, BuildConfig.API_KEY)
                        .build();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);
                Log.d(TAG, url.toString());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        return httpClient.build();
    }


}