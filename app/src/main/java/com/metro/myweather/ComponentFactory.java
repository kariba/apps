package com.metro.myweather;

import com.metro.myweather.networking.ApiServicesModule;

/**
 * Created by ekkamau on 11/6/17.
 */

public class ComponentFactory {

    public static final MyWeatherComponent create() {

        return DaggerMainAppComponent.builder()
                .apiServicesModule(new ApiServicesModule())
                .build();
    }
}