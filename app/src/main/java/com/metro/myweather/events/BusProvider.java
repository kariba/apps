package com.metro.myweather.events;

import android.os.Handler;
import android.os.Looper;

import com.metro.myweather.MainThreadBus;

/**
 * Created by ekkamau on 11/6/17.
 */

public final class BusProvider {
    private static final MainThreadBus BUS = new MainThreadBus();

    private BusProvider() {
    }

    public static MainThreadBus getInstance() {
        return BUS;
    }

    public static void postOnMainThread(final Object event) {
        Handler handler = new Handler(Looper.getMainLooper());

        handler.post(new Runnable() {
            public void run() {
                BUS.post(event);
            }
        });
    }
}