package com.metro.myweather.events;


import com.metro.myweather.currentweather.CurrentWeatherResponse;

/**
 * Created by ekkamau on 11/6/17.
 */

public class CurrentWeatherEvent {
    CurrentWeatherResponse weatherResponse;

    public CurrentWeatherEvent(CurrentWeatherResponse weatherResponse) {
        this.weatherResponse = weatherResponse;
    }
}
