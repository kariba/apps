package com.metro.myweather.events;

/**
 * Created by ekkamau on 11/6/17.
 */

public class GenericErrorEvent {
    String errorMessage;

    public GenericErrorEvent(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
