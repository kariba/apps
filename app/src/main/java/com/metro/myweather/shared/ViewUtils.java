package com.metro.myweather.shared;

import android.widget.TextView;

import com.metro.myweather.BuildConfig;

/**
 * Created by ekkamau on 11/7/17.
 */

public class ViewUtils {

    public static String getIconUrl(String code) {
        return String.format(BuildConfig.DEFAULT_BASE_URL, code);
    }

    public static void setTextViewContent(TextView textView, Object content) {
        if (content != null) {
            textView.setText(String.valueOf(content));
        }
    }


}
