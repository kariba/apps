package com.metro.myweather.shared;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ekkamau on 11/7/17.
 */

public class Utils {

    private static final String DATE_FROMAT = "dd MMMM YYYY";

    public static String formattedDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FROMAT);
        return df.format(date);
    }

    public static double kelvinToCelcius(double temperatue) {
        return temperatue - 273.15;
    }
}
