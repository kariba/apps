package com.metro.myweather.components;

import com.metro.myweather.ActivityScope;
import com.metro.myweather.MainActivity;
import com.metro.myweather.modules.CurrentWeatherServiceModule;

import dagger.Subcomponent;

/**
 * Created by ekkamau on 11/6/17.
 */

@ActivityScope
@Subcomponent(modules = {
        CurrentWeatherServiceModule.class
})
public interface CurrentWeatherComponent {
    void inject(MainActivity activity);
}