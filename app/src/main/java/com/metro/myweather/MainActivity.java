package com.metro.myweather;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.metro.myweather.currentweather.CurrentWeatherResponse;
import com.metro.myweather.currentweather.CurrentWeatherService;
import com.metro.myweather.location.BaseAbstractLocationActivity;
import com.metro.myweather.modules.CurrentWeatherServiceModule;
import com.metro.myweather.shared.Utils;
import com.metro.myweather.shared.ViewUtils;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseAbstractLocationActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    @Inject
    CurrentWeatherService currentWeatherService;
    @BindView(R.id.todayDate)
    TextView todayDateView;
    @BindView(R.id.weatherIcon)
    ImageView myweatherIcon;
    @BindView(R.id.highestTemp)
    TextView highestTempView;
    @BindView(R.id.lowestTemp)
    TextView lowestTempView;
    @BindView(R.id.locationName)
    TextView locationNameView;

    long requestId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        MyWeatherApp.get(getApplicationContext())
                .getAppComponent()
                .currentWeatherComponent(new CurrentWeatherServiceModule())
                .inject(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchLocation(mLastLocation);
    }

    @Subscribe
    public void onCurrentWeatherResponse(CurrentWeatherResponse currentWeatherResponse) {
        if (requestId == currentWeatherResponse.getRequestId()) {
            ArrayList<CurrentWeatherResponse.Weather> weathers = currentWeatherResponse.getWeather();
            if (weathers != null && !weathers.isEmpty()) {
                CurrentWeatherResponse.Weather weather = weathers.get(0);
                Log.d(TAG, "here");
                Picasso.with(this).load(ViewUtils.getIconUrl(weather.getIcon()));
            }
            CurrentWeatherResponse.Main main = currentWeatherResponse.getMain();
            ViewUtils.setTextViewContent(highestTempView, getString(R.string.max_temp, Utils.kelvinToCelcius(main.getTempMax())));
            ViewUtils.setTextViewContent(lowestTempView, getString(R.string.min_temp, Utils.kelvinToCelcius(main.getTempMin())));
            ViewUtils.setTextViewContent(todayDateView, getString(R.string.today_date, Utils.formattedDate(Calendar.getInstance().getTime())));
        }

    }

    @Override
    protected void onLocationReturned() {
        fetchLocation(mLastLocation);
    }

    private void fetchLocation(Location location) {
        if (location != null) {
            requestId = currentWeatherService.getCurrentWeatherByLatitudeAndLongitude(bus, location.getLatitude(), location.getLongitude());
        }
    }
}
