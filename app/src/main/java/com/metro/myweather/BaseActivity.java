package com.metro.myweather;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.metro.myweather.events.BusProvider;
import com.metro.myweather.events.GenericErrorEvent;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class BaseActivity extends AppCompatActivity {
    public static final String TAG = BaseActivity.class.getSimpleName();
    protected Bus bus = BusProvider.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Subscribe
    public void onGenericErrorEvent(GenericErrorEvent
                                            genericErrorEvent) {
        Log.d(TAG, genericErrorEvent.getErrorMessage());
    }
}
