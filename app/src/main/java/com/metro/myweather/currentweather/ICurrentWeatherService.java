package com.metro.myweather.currentweather;

import com.squareup.otto.Bus;

/**
 * Created by ekkamau on 11/6/17.
 */

public interface ICurrentWeatherService {

    long getCurrentWeatherByLatitudeAndLongitude(Bus bus, double latitude, double longitude);
}
