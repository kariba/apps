package com.metro.myweather.currentweather;

/**
 * Created by ekkamau on 11/6/17.
 */

public class CurrentWeatherRequest {

    String lat, lon;

    public CurrentWeatherRequest(String lat, String lon) {
        this.lat = lat;
        this.lon = lon;
    }
}
