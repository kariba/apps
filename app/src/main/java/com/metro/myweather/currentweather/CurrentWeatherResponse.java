package com.metro.myweather.currentweather;

import com.google.gson.annotations.SerializedName;
import com.metro.myweather.networking.BaseResponse;

import java.util.ArrayList;

/**
 * Created by ekkamau on 11/6/17.
 */

public class CurrentWeatherResponse extends BaseResponse {

    private Coord coord;
    private Sys sys;
    private ArrayList<Weather> weather;
    private Main main;
    private Wind wind;
    private Rain rain;
    private Clouds clouds;
    private int dt;
    private int id;
    private String name;
    private int cod;

    public CurrentWeatherResponse(long requestId) {
        super(requestId);
    }

    public Coord getCoord() {
        return this.coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Sys getSys() {
        return this.sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public ArrayList<Weather> getWeather() {
        return this.weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return this.main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Wind getWind() {
        return this.wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Rain getRain() {
        return this.rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    public Clouds getClouds() {
        return this.clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public int getDt() {
        return this.dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return this.cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }


    public static class Coord {
        private int lon;
        private int lat;

        public int getLon() {
            return this.lon;
        }

        public void setLon(int lon) {
            this.lon = lon;
        }

        public int getLat() {
            return this.lat;
        }

        public void setLat(int lat) {
            this.lat = lat;
        }
    }

    public static class Sys {
        private String country;
        private int sunrise;
        private int sunset;

        public String getCountry() {
            return this.country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public int getSunrise() {
            return this.sunrise;
        }

        public void setSunrise(int sunrise) {
            this.sunrise = sunrise;
        }

        public int getSunset() {
            return this.sunset;
        }

        public void setSunset(int sunset) {
            this.sunset = sunset;
        }
    }

    public static class Weather {
        private int id;
        private String main;
        private String description;
        private String icon;

        public int getId() {
            return this.id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMain() {
            return this.main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getDescription() {
            return this.description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIcon() {
            return this.icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }

    public static class Main {
        private double temp;
        private int humidity;
        private double pressure;
        private double temp_min;
        private double temp_max;

        public double getTemp() {
            return this.temp;
        }

        public void setTemp(double temp) {
            this.temp = temp;
        }

        public int getHumidity() {
            return this.humidity;
        }

        public void setHumidity(int humidity) {
            this.humidity = humidity;
        }

        public double getPressure() {
            return this.pressure;
        }

        public void setPressure(double pressure) {
            this.pressure = pressure;
        }

        public double getTempMin() {
            return this.temp_min;
        }

        public void setTempMin(double temp_min) {
            this.temp_min = temp_min;
        }

        public double getTempMax() {
            return this.temp_max;
        }

        public void setTempMax(double temp_max) {
            this.temp_max = temp_max;
        }
    }

    public static class Wind {
        private double speed;
        private double deg;

        public double getSpeed() {
            return this.speed;
        }

        public void setSpeed(double speed) {
            this.speed = speed;
        }

        public double getDeg() {
            return this.deg;
        }

        public void setDeg(double deg) {
            this.deg = deg;
        }
    }

    public static class Rain {
        @SerializedName("3h")
        private int threeHour;

        public int getThreeHour() {
            return threeHour;
        }
    }

    public static class Clouds {
        private int all;

        public int getAll() {
            return this.all;
        }

        public void setAll(int all) {
            this.all = all;
        }
    }


}
