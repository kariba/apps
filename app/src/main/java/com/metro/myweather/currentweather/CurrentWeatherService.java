package com.metro.myweather.currentweather;

import com.metro.myweather.networking.IApiService;
import com.metro.myweather.networking.RequestIdService;
import com.metro.myweather.networking.RestCallback;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ekkamau on 11/6/17.
 */

public class CurrentWeatherService implements ICurrentWeatherService {

    IApiService apiService;

    @Inject
    public CurrentWeatherService(IApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public long getCurrentWeatherByLatitudeAndLongitude(final Bus bus, double latitude, double longitude) {
        final long requestId = RequestIdService.getNewRequestId();
        apiService.getCurrentWeatherByLatLong(latitude, longitude).enqueue(new RestCallback<CurrentWeatherResponse>(bus, requestId) {
            @Override
            public void onResponse(Call<CurrentWeatherResponse> t, Response<CurrentWeatherResponse> response) {
                super.onResponse(t, response);
                CurrentWeatherResponse apiResponse = response.body();
                if (apiResponse != null) {
                    apiResponse.setRequestId(requestId);
                }
                bus.post(apiResponse);
            }
        });
        return requestId;
    }
}
