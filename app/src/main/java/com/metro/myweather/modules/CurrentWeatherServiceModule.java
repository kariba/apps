package com.metro.myweather.modules;

import com.metro.myweather.currentweather.CurrentWeatherService;
import com.metro.myweather.currentweather.ICurrentWeatherService;
import com.metro.myweather.networking.IApiService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ekkamau on 11/6/17.
 */

@Module
public class CurrentWeatherServiceModule {

    public CurrentWeatherServiceModule() {

    }

    @Provides
    public ICurrentWeatherService providesCurrentWeatherService(IApiService apiService) {

        return new CurrentWeatherService(apiService);

    }
}
