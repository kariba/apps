package com.metro.myweather.modules;

import com.metro.myweather.MyWeatherApp;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ekkamau on 11/6/17.
 */

@Module
public class AppModule {

    private MyWeatherApp app;

    public AppModule(MyWeatherApp app) {
        this.app = app;
    }

    @Provides
    public MyWeatherApp provideApplication() {
        return app;
    }
}
