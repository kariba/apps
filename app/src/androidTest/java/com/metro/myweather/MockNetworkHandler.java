package com.metro.myweather;


import java.io.IOException;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

/**
 * Created by ekkamau on 11/6/17.
 */

public class MockNetworkHandler {

    static MockWebServer server = new MockWebServer();

    public static void start() {
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stop() {
        try {
            server.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void responseFromJson(String file) throws Exception {
        MockResponse mockResponse = new MockResponse().setResponseCode(200).setBody(new FileUtils().getFileContent(file));
        server.enqueue(mockResponse);
    }


}

