package com.metro.myweather;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by ekkamau on 11/6/17.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest extends BaseTest{


    @Before
    public void setUp() throws Exception {
        super.setUp();
        MockNetworkHandler.responseFromJson("current_weather.json");
    }

    @Test
    public void can_handle_response()throws Exception{
        launchActivity(MainActivity.class);
        Thread.sleep(10000);
    }
}
