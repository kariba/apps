package com.metro.myweather;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;

import org.junit.Before;

/**
 * Created by ekkamau on 11/6/17.
 */

public class BaseTest {


    @Before
    public void setUp()throws Exception {
    }

    public void launchActivity(Class clazz) {
        new ActivityTestRule<>(clazz).launchActivity(new Intent());
    }
}
