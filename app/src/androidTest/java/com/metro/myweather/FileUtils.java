package com.metro.myweather;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by ekkamau on 11/6/17.
 */

public class FileUtils {


    private File getFile(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }

    public String getFileContent(String fileName) {

        StringBuilder result = new StringBuilder("");

        String strLine;
        try  {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(fileName), "UTF-8"));
            while ((strLine = reader.readLine()) != null) {
                result.append(strLine);
            }
        } catch (final IOException ignore) {
            //ignore
        }
        return result.toString();

    }
}
